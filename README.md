# XMC
Repository for XMC Minceraft Launcher

# Installation :zap:
```npm
git clone https://gitlab.com/DaMotherYeeter/xmc.git
cd xmc
npm install
```

# Run :rocket:
```npm
npm start
```

# Credits
Zeldown / EMC-Core
